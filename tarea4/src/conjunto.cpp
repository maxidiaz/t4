/* 5220298 - 5034511  */

#include "../include/info.h"
#include "../include/cadena.h"
#include "../include/binario.h"
#include "../include/conjunto.h"
#include "../include/iterador.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

struct rep_conjunto{
  binario_t binario;
};

/* Constructoras */

/*  Devuelve un conjunto_t vacío (sin elementos). */
conjunto_t crear_conjunto()
{
  conjunto_t nuevo = new rep_conjunto;
  nuevo->binario = crear_binario();
  return nuevo;
}

/*
  Devuelve un conjunto_t con los mismos elementos que están en `cad'.
  El conjunto_t devuelto no comparte memoria con `cad'.
  Precondición: los elementos de `cad' están en orden lexicográfico creciente
  estricto (esto es, creciente y no hay dos iguales) según sus frases.
  El tiempo de ejecución en peor caso es O(n.log n), donde `n' es la cantidad
  de elementos de `cad'.
  Mientras en el conjunto_t devuelto no se hagan inserciones ni remociones las
  siguientes llamadas a `pertenece' deben ejecutarse en tiempo O(log n) en peor
  caso.
 */
conjunto_t construir_conjunto(cadena_t cad)
{
  conjunto_t nuevo = new rep_conjunto;
  nuevo->binario=crear_balanceado(cad);
  return nuevo;
}

/*
  Incluye `i' en `c'.
  Si en `c' ya había un elemento con frase igual a la de `i' no hace nada.
  Devuelve `true' si y sólo si se incluyó `i'.
  El tiempo de ejecución es O(log n) en promedio, donde `n' es la cantidad de
  elementos de `c'.
 */
bool incluir(info_t i, conjunto_t &c)
{
  return insertar_en_binario(i,c->binario);
}



/*funcion auxiliar para union conjunto, que dadas dos cadenas ordenadas c1 y c2,
me devuelve una cadena con todos los elementos de c1 y c2, ordenados y SIN repetir

muy similar a la funcion mezcla en cadena.cpp*/

static cadena_t merge_sin_rep(cadena_t c1, cadena_t c2) 
{
  cadena_t nueva = crear_cadena();
  localizador_t loc1 = inicio_cadena(c1);
  localizador_t loc2 = inicio_cadena(c2);
   while (es_localizador(loc1) && es_localizador(loc2))
    {
        //si el numero de loc1 es menor o igual al de loc2, entonces en inserto una copia del info de loc1 al final de nuevo
        if (strcmp(frase_info(info_cadena(loc1, c1)), frase_info(info_cadena(loc2, c2)))<0)
        {
            insertar_al_final(copia_info(info_cadena(loc1, c1)), nueva); 
            //avanzo loc1
            loc1 = siguiente(loc1, c1);
        }
        //si no, entonces inserto una copia del info de loc2 al final de nuevo
        else
        {
          if (strcmp(frase_info(info_cadena(loc1, c1)), frase_info(info_cadena(loc2, c2)))==0)
          {
            insertar_al_final(copia_info(info_cadena(loc1, c1)), nueva); 
            //avanzo loc1
            loc1 = siguiente(loc1, c1);
            loc2 = siguiente(loc2,c2);
          }
          else
          {
            insertar_al_final(copia_info(info_cadena(loc2, c2)), nueva);
            //avanzo loc2
            loc2 = siguiente(loc2, c2);
          }
        }
    }
    //si llegue al final de c1, sigo agregando los elementos de c2
    if (!es_localizador(loc1)) 
    { while (es_localizador(loc2)) 
        {
            insertar_al_final(copia_info(info_cadena(loc2, c2)), nueva);
            loc2 = siguiente(loc2, c2);
        }
    }
    else
    {
      while (es_localizador(loc1)) 
      {
          insertar_al_final(copia_info(info_cadena(loc1, c1)), nueva); 
          loc1 = siguiente(loc1, c1);
      }
    }
 return nueva;
}  

/*
  Devuelve un conjunto_t con los elementos cuyas frases pertenecen a `c1'
  o `c2'. Si en ambos conjuntos hay un elemento con la misma frase en el
  conjunto_t devuelto se debe incluir el que pertenece a `c1'.
  El tiempo de ejecucion en peor caso es O(n1 + n2 + n.log n), siendo `n1' y
  `n2' la cantidad de elementos de `c1' y `c2' respectivamente y `n' la del
  conjunto_t resultado.
  Mientras en el conjunto_t devuelto no se hagan inserciones ni remociones las
  siguientes llamadas a `pertenece' deben ejecutarse en tiempo O(log n) en peor
  caso.
  El conjunto_t devuelto no comparte memoria ni con `c1' no con `c2'.

 */

conjunto_t union_conjunto(conjunto_t s1, conjunto_t s2)
{
  cadena_t c1 = linealizacion(s1->binario);
  cadena_t c2 = linealizacion(s2->binario);
  cadena_t resultado = merge_sin_rep(c1,c2);

  conjunto_t unido = construir_conjunto(resultado);

  liberar_cadena(c1);
  liberar_cadena(c2);
  liberar_cadena(resultado);
  
  return unido;

}


/*Pone los elementos que estan en ambas cadenas, c1 y c2, en orden 
lexicografico creciente. Tiene O(n1 + n2), donde n1 es el tamanio
de c1 y n2 el tamanio de c2 */

static cadena_t interseccion_cadena(cadena_t cad1, cadena_t cad2)
{
  cadena_t nueva = crear_cadena();
  localizador_t loc1 = inicio_cadena(cad1);
  localizador_t loc2 = inicio_cadena(cad2);
  while(es_localizador(loc1)&&es_localizador(loc2))
  {
    if (strcmp(frase_info(info_cadena(loc1,cad1)),frase_info(info_cadena(loc2,cad2)))==0)
    {
      insertar_al_final(copia_info(info_cadena(loc1,cad1)),nueva);
      loc1=siguiente(loc1,cad1);
      loc2=siguiente(loc2,cad2);
    }
    else if(strcmp(frase_info(info_cadena(loc1,cad1)),frase_info(info_cadena(loc2,cad2)))>0)
      loc2=siguiente(loc2,cad2);
    else
      loc1=siguiente(loc1,cad1);
  }

  return nueva;

}

/*
  Devuelve un conjunto_t con los elementos cuyas frases pertenecen a `c1'
  y `c2'. El  dato numérico en el conjunto_t devuelto debe ser el del elemento
  de `c1'-
  El tiempo de ejecucion en peor caso es O(n1 + n2 + n.log n), siendo `n1' y
  `n2' la cantidad de elementos de `c1' y `c2' respectivamente y `n' la del
  conjunto_t resultado.
  Mientras en el conjunto_t devuelto no se hagan inserciones ni remociones las
  siguientes llamadas a `pertenece' deben ejecutarse en tiempo O(log n) en peor
  caso.
  El conjunto_t devuelto no comparte memoria ni con `c1' no con `c2'.
 */
conjunto_t interseccion(conjunto_t c1, conjunto_t c2)
{
  cadena_t cad1 = linealizacion(c1->binario);
  cadena_t cad2 = linealizacion(c2->binario);
  cadena_t inter_cadena=interseccion_cadena(cad1,cad2);

  conjunto_t inter_conjunto = construir_conjunto(inter_cadena);
  
  liberar_cadena(cad1);
  liberar_cadena(cad2);
  liberar_cadena(inter_cadena);
  

  return inter_conjunto;
}


/*funcion auxiliar para diferencia, que dadas dos cadenas cad1 y cad2,
me devuelve una cadena con los elementos de cad1 que NO estan en cad2*/

static cadena_t diferencia_cadena(cadena_t cad1, cadena_t cad2)
{
  cadena_t nueva = crear_cadena();
  localizador_t loc1 = inicio_cadena(cad1);
  localizador_t loc2 = inicio_cadena(cad2);
  while(es_localizador(loc1)&&es_localizador(loc2))
  {
    if (strcmp(frase_info(info_cadena(loc1,cad1)),frase_info(info_cadena(loc2,cad2)))==0)
    {  
      loc1=siguiente(loc1,cad1);
      loc2 =siguiente(loc2,cad2);
    }
    else 
    {
      if (strcmp(frase_info(info_cadena(loc1,cad1)),frase_info(info_cadena(loc2,cad2)))<0)
      {
        insertar_al_final(copia_info(info_cadena(loc1,cad1)),nueva);
        loc1=siguiente(loc1,cad1);
      }
      else
      {
        localizador_t aux= loc2;
        while (es_localizador(aux) && (strcmp(frase_info(info_cadena(loc1,cad1)),frase_info(info_cadena(aux,cad2)))>0) )
          aux = siguiente(aux,cad2);
        if (es_localizador(aux))
        {
          if (strcmp(frase_info(info_cadena(loc1,cad1)),frase_info(info_cadena(aux,cad2)))==0)
          {
            loc1=siguiente(loc1,cad1);
            loc2=siguiente(aux,cad2);

          } 
          else 
          {
            insertar_al_final(copia_info(info_cadena(loc1,cad1)),nueva);
            loc1=siguiente(loc1,cad1);
          }
        }

      }

    }
  }
  while (es_localizador(loc1))
  {
    insertar_al_final(copia_info(info_cadena(loc1,cad1)),nueva);
    loc1=siguiente(loc1,cad1);
  }


  return nueva;

} 

/*
  Devuelve un conjunto_t con los elementos de `c1' cuyos datos de texto no
  pertenecen a `c2'.
  El tiempo de ejecucion en peor caso es O(n1 + n2 + n.log n), siendo `n1' y
  `n2' la cantidad de elementos de `c1' y `c2' respectivamente y `n' la del
  conjunto_t resultado.
  Mientras en el conjunto_t devuelto no se hagan inserciones ni remociones las
  siguientes llamadas a `pertenece` deben ejecutarse en tiempo O(log n) en peor
  caso.
  El conjunto_t devuelto no comparte memoria ni con `c1' no con `c2'.
 */
conjunto_t diferencia(conjunto_t c1, conjunto_t c2)
{
  cadena_t cad1 = linealizacion(c1->binario);
  cadena_t cad2 = linealizacion(c2->binario);
  cadena_t dif = diferencia_cadena(cad1,cad2);
  
  conjunto_t dif_conj = construir_conjunto(dif);
  
  liberar_cadena(cad1);
  liberar_cadena(cad2);
  liberar_cadena(dif);
  
  return dif_conj;
}

/* Destructoras */

/*
  Excluye de `c' el elemento cuyo dato de texto es `t' y libera la memoria
  asignada a ese elemento.
  Si en `c' no había tal elemento no hace nada.
  El tiempo de ejecución es O(log n) en promedio, donde `n' es la cantidad de
  elementos de `c'.
 */
void excluir(char *t, conjunto_t &c)
{
    bool elimine = remover_de_binario(t,c->binario);
    if (elimine)
    {}
  // para que no aparezca el error: unused variable 'elimine'
}

/* Libera la memoria asignada a `c' y la de todos sus elementos. */
void liberar_conjunto(conjunto_t &c)
{
  liberar_binario(c->binario);
  delete c;
  c= NULL;
}

/* Predicados */

/*
  Devuelve `true' si y sólo si el dato de texto de algún elemento de `c' es
  igual a `t'.
  El tiempo de ejecución es O(log n) en promedio, donde `n' es la cantidad de
  elementos de `c'.
 */
bool pertenece_conjunto(char *t, conjunto_t c)
{
  binario_t sub_con_t = buscar_subarbol(t,c->binario);
  //si t pertenece al arbol, entonces buscar_subarbol me devuelve el subarbol 
  //donde la raiz es t. Si no estaba,devuelve NULL. 
  return (!es_vacio_binario(sub_con_t));
}

/*
  Devuelve `true' si y sólo `c' es vacío (no tiene elementos).
  El tiempo de ejecución es O(1).
 */
bool es_vacio_conjunto(conjunto_t c)
{
  return ((c==NULL)||es_vacio_binario(c->binario));
}

/* Selectoras */

/*
  Devuelve un iterador a las frases de los elementos de `c'.
  En la recorrida del iterador devuelto los textos aparecerán en orden
  lexicográfico creciente.
  El tiempo de ejecución es O(n), donde `n' es la cantidad de elementos de `c'.
  El iterador resultado NO comparte memoria con `c'.
 */
iterador_t iterador_conjunto(conjunto_t c)
{
  iterador_t iter = crear_iterador();

  cadena_t cad = linealizacion(c->binario);
  localizador_t loc = inicio_cadena(cad);

  while (es_localizador(loc))
  {
    char* str = new char[strlen(frase_info(info_cadena(loc,cad))) +1]; // precisa ser inicializado
    strcpy(str,frase_info(info_cadena(loc,cad)));
    agregar_a_iterador(str,iter);
    loc = siguiente(loc,cad);
  }
    liberar_cadena(cad);
  reiniciar_iter(iter);
  //dejo que la posicion actual en el primero. Estara bien? No lo se

  return iter;
  
  
}
