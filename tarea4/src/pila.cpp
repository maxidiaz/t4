/* 5220298 - 5034511  */

#include "../include/pila.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

//FUNCIONA
struct nodo_pila{
    char *str;
    nodo_pila *sig;
};

typedef nodo_pila *lista_pila;

struct rep_pila{
    lista_pila stack;
    int tam_max;
    int tam_actual;
};




/* Constructoras */

/*
  Devuelve una pila_t vacía (sin elementos) que puede tener hasta `tamanio'
  elementos.
 */
pila_t crear_pila(int tamanio)
{
    pila_t nuevo = new rep_pila;
    nuevo->stack = NULL;
    nuevo->tam_actual = 0;
    nuevo->tam_max = tamanio;
    return nuevo;
}

/*
  Apila`t' en `p'.
  Si es_llena_pila(p) no hace nada.
 */
void apilar(char *t, pila_t &p)
{
    if(!es_llena_pila(p))
    {
      lista_pila nuevo = new nodo_pila;
      nuevo->str = t;
      nuevo->sig = p->stack;
      p->stack = nuevo;
      p->tam_actual = p->tam_actual +1;
    }
}

/* Destructoras */

/*
  Remueve de `p' el elemento que está en la cima.
  Libera la memoria del elemento removido.
  Si es_vacia_pila(p) no hace nada.
 */
void desapilar(pila_t &p)
{
  if(!es_vacia_pila(p))
  {
    lista_pila a_borrar = p->stack;
    p->stack= a_borrar->sig;
    delete[] a_borrar->str;
    delete a_borrar; 
    p->tam_actual = p->tam_actual -1;
  }
}
/*Libera la memoria asociada a l recursivamente, eliminando tambien el string de nodo_pila*/

static void liberar_lista_pila(lista_pila &l)
{
  if(l != NULL)
  {
    liberar_lista_pila(l->sig);
    delete[] l->str;
    delete l;
  }

}
/* Libera la memoria asignada a `p' y la de todos sus elementos. */
void liberar_pila(pila_t &p)
{
  liberar_lista_pila(p->stack);
  delete p;
  p= NULL;

}

/* Predicados */

/* Devuelve `true' si y sólo si `p' es vacía (no tiene elementos). */
bool es_vacia_pila(pila_t p)
{
  return ((p == NULL)||(p->tam_actual == 0));
}

/*
  Devuelve `true' si y sólo si la cantidad de elementos en `p' es `tamanio'
  (siendo `tamanio' el valor del parámetro con que fue creada `p').
 */
bool es_llena_pila(pila_t p)
{
    return (p->tam_max == p->tam_actual);
}

/* Selectoras */

/*
  Devuelve el elemento que está en la cima de `p'.
  Precondición: ! es_vacia_pila(p);
 */
char *cima(pila_t p)
{
  return p->stack->str;	
}