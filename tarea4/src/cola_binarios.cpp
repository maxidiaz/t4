/* 5220298 - 5034511  */
/*
  Módulo de definición de `cola_binarios_t'.

  `cola_binarios_t' es una estructura lineal con comportamiento FIFO cuyos
   elementos son de tipo `binario'.

  Laboratorio de Programación 2.
  InCo-FIng-UDELAR
 */
#include "../include/cola_binarios.h"
#include "../include/binario.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

struct nodo_cola {
  binario_t bin;
  nodo_cola *sig;

};

typedef struct nodo_cola *cola ;

struct rep_cola_binarios{
    nodo_cola *inicio, *final;
};


/* Constructoras */



/*  Devuelve una cola_binarios_t vacía (sin elementos). */
cola_binarios_t crear_cola_binarios()
{
  cola_binarios_t colavacia = new rep_cola_binarios;
  colavacia->inicio=NULL;
  colavacia->final=NULL;
  return colavacia;
}

/* Encola `b' en `c'. */
void encolar(binario_t b, cola_binarios_t &c)
{
  //creo el nodo
  cola nuevo = new nodo_cola;
  nuevo->bin=b;
  nuevo->sig=NULL;
  //si la lista es vacia, el inicio va a ser el nuevo elemento
  if (es_vacia_cola_binarios(c))
    c->inicio=nuevo;
  //si no, agrego el nuevo elemento al final
  else 
    c->final->sig=nuevo;
  //el final de la lista pasa a ser el elemento que agregue
  c->final=nuevo;

}

/* Destructoras */

/*
  Remueve de `c' el elemento que está en el frente.
  No libera la memoria del elemento removido.
  Si es_vacia_cola_binarios(c) no hace nada.
 */
void desencolar(cola_binarios_t &c)
{
  if (!es_vacia_cola_binarios(c))
  {
    cola borrar=c->inicio;
    //si solo tiene un elemento, el final queda en NULL
    if (c->inicio==c->final)
      c->final=NULL; 
    //el inicio pasa a ser el siguiente
    c->inicio=c->inicio->sig;
    delete borrar;
  }

}

static void borrar_cola(cola &c)
{
  if (c!=NULL)
  {
    borrar_cola(c->sig);
    delete c;
  }
}

/* Libera la memoria asignada a `c', pero NO la de sus elementos. */
void liberar_cola_binarios(cola_binarios_t &c)
{
  borrar_cola(c->inicio);
  delete c;
  c=NULL;
}


/* Predicados */

/* Devuelve `true' si y sólo si `c' es vacía (no tiene elementos). */
bool es_vacia_cola_binarios(cola_binarios_t c)
{
  return ((c==NULL)||(c->inicio==NULL));
}

/* Selectoras */

/*
  Devuelve el elemento que está en el frente de `c'.
  Precondición: ! es_vacia_cola_binarios(c);
 */
binario_t frente(cola_binarios_t c)
{
  return c->inicio->bin;
}