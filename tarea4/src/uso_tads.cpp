/* 5220298 - 5034511  */


#include "../include/info.h"
#include "../include/cadena.h"
#include "../include/cola_binarios.h"
#include "../include/pila.h"
#include "../include/binario.h"
#include "../include/conjunto.h"



#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>



/*Funcion auxiliar para inverso_de_iter. cant va contando la cantidad de 
elementos que me falta agregar. cant cuenta la cantidad de elementos que 
tengo que agregar(Empieza en (b-a)). Final es el contador LOCAL que dice hasta donde 
tengo que ir en la recursion.
Tiene O(n^2) */

static void inverso_acotado(nat &cant, iterador_t it, iterador_t &nuevo_iter, nat b)
{
  if ((cant != 0)&&(esta_definida_actual(it)))
  {
    reiniciar_iter(it);
    nat i = 1;
    while ((i < b )&&(hay_siguiente_en_iter(it)))
    {
      avanzar_iter(it);
      i++;
    }//avanzo hasta la posicion que quiero agregar

     //llegue al mas a la derecha posible que queria agregar ahora.
    char* str_actual = new char[strlen(actual_iter(it)) +1];
    strcpy(str_actual , actual_iter(it));
    agregar_a_iterador(str_actual,nuevo_iter);

    cant--;
    //como ya agregue un elemento, tengo que agregar uno menos ahora.
    b--;
    
    //ahora voy a querer agregar el elemento anterior al que agregue recien
    inverso_acotado(cant, it, nuevo_iter,b);



  }
}

/*
  Devuelve un iterador_t con elementos de `it' en orden inverso de como están
  en `it'.
  Si `a' es mayor que la cantidad de elementos de `it' devuelve un iterador_t
  vacio. En otro caso devuleve los que están entre las posiciones `a' y `b' o,
  si `b' es mayor que la cantidad de elementos_ , entre `a' y el último elemento
  de `it'.
  Se considera que el primer elemento está en la posición 1.
  El iterador_t devuelto no debe compartir memoria con `it'.
  Al terminar `it' y el iterador_t devuelte deben quedar reiniciados.
  Precondición: 1 <= a <= b.

  No se pueden usar operaciones de un TAD que no sea iterador.

 */
iterador_t inverso_de_iter(nat a, nat b, iterador_t it)
{
  reiniciar_iter(it); //voy al primer elemento
  if (esta_definida_actual(it))
  { 
    nat long_iter = 1;
    while (hay_siguiente_en_iter(it))
    {
      avanzar_iter(it);
      long_iter++;
    }//calculo la "longitud" del iterador
    if (a > long_iter) 
    {
      reiniciar_iter(it);
      return crear_iterador(); // si tengo que empezar en a, pero no hay a elementos, devuelvo vacia
    }
    else
    {
      if (b > long_iter)
        b = long_iter;// no tiene sentido ir hasta la pos b si no existe.
      
        
      nat cuenta = b - a + 1; // cuenta es la cantidad de elementos que quiero
      iterador_t nuevo = crear_iterador();
      inverso_acotado (cuenta,it,nuevo,b);
      
      reiniciar_iter(it);
      reiniciar_iter(nuevo);
      return nuevo;
    }
  }// esta_definida_actual(it)
  else
    return crear_iterador();
}

/*
  Devuelve un iterador_t con los elementos de `c' que están entre `primero' y
  `ultimo', ambos incluidos.
  El iterador_t resultado debe estar en orden lexicográfico decreciente.
  Precondición: `primero' precede o es igual a `ultimo' en orden lexicográfico.
 */
iterador_t rango_en_conjunto(char *primero, char *ultimo, conjunto_t c)
{
  iterador_t aux = iterador_conjunto(c);
  iterador_t res;
  nat inicio = 0;
  reiniciar_iter(aux);
  if (esta_definida_actual(aux))
  {
    inicio=1;
    while (esta_definida_actual(aux) && strcmp(actual_iter(aux),primero)<0)
    { avanzar_iter(aux);
      inicio++;
    }
  
    nat final = inicio; 
    while (esta_definida_actual(aux) && strcmp(actual_iter(aux),ultimo)<0)
    {
     avanzar_iter(aux); 
     final++;
    }
    res = inverso_de_iter(inicio, final, aux);
  }
  else res = crear_iterador();
  liberar_iterador(aux);
  return res; 
}

/*
  Devuelve `true' si y sólo si `cad' está ordenada de forma creciente
  (creciente de manera estricta) según las frases de sus elementos.
  Si es_vacia_cadena (cad) devuelve `true'.
 */
bool esta_ordenada_por_frase(cadena_t cad)
{
  bool res = true;
    if (!es_vacia_cadena(cad)) 
    {
      localizador_t loc = inicio_cadena(cad);
      while (res && es_localizador(siguiente(loc, cad))) {
          localizador_t sig_loc = siguiente(loc, cad);
          if (strcmp(frase_info(info_cadena(loc, cad)),frase_info(info_cadena(sig_loc, cad))) >=0)
              res = false; 
          else 
              loc = siguiente(loc, cad);
      }
    }
  return res;
}

/*funcion auxiliar para imprimir_por_niveles*/

static void aux_imprimir(cola_binarios_t &cola, binario_t b, unsigned int cant)
{
  if (!es_vacio_binario(b))
  { if (cant==1)
      encolar(b, cola);
    else
    {
      aux_imprimir(cola, izquierdo(b), cant-1);
      aux_imprimir(cola, derecho(b), cant-1);
    }
  }
}

/*
  Imprime las frases de los nodos de `b' en orden decreciente de
  niveles (desde las hojas hasta la raíz), un nivel por línea.
  En cada nivel los textos se imprimen en orden lexicográfico creciente.
  Después de cada texto se imprime un espacio en blanco.
  Si `b' es vacío no imprime nada.
 */
void imprimir_por_niveles(binario_t b)
{
  if (!es_vacio_binario(b))
  {
    unsigned int cuenta = altura_binario(b);
    cola_binarios_t aux_cola = crear_cola_binarios();
    while (cuenta>0)
    {
      aux_imprimir(aux_cola, b, cuenta);
      while (!es_vacia_cola_binarios(aux_cola))
      {
        binario_t aimprimir = frente(aux_cola);
        desencolar(aux_cola);
        printf("%s ", frase_info(raiz(aimprimir))); 
      }
      if (cuenta !=1)
        printf("\n");
      cuenta--;
      
    }
    
    liberar_cola_binarios(aux_cola);
    
  }

}

/*
  Devuelve `true' si y sólo si en `cad' hay un elemento cuyo dato numérico es
  `i'.
 */
bool pertenece(int i, cadena_t cad){
  if (!es_vacia_cadena(cad)){
    localizador_t loc = inicio_cadena(cad); //se mueve solo el localizador
    while((es_localizador(loc))&&(numero_info(info_cadena(loc,cad)) != i))
      loc=siguiente(loc,cad);
    return es_localizador(loc); //si es null es porque llegue al final, entonces no pertenece
  }//if
  else return false;
}

/*  Devuelve la cantidad de elementos de `cad'. */
nat longitud(cadena_t cad){
  nat i=0;
  localizador_t loc = inicio_cadena(cad);
  while(es_localizador(loc)){ 
    loc=siguiente(loc,cad);
    i++;
  }//while
  return i;
}

/*
  Devuelve `true' si y sólo si `c1' y `c2' son iguales (es decir, si los
  elementos son iguales y en el mismo orden).
  Si es_vacia_cadena(c1) y es_vacia_cadena(c2) devuelve `true'.
 */
bool son_iguales(cadena_t c1, cadena_t c2){
  if (es_vacia_cadena(c1) && es_vacia_cadena(c2)) 
    return true;
  else if (es_vacia_cadena(c1) || es_vacia_cadena(c2))
    return false;
  else { //aca se se sabe que ninguna de las dos es vacia
    localizador_t loc1 = inicio_cadena(c1);
    localizador_t loc2 = inicio_cadena(c2);

    while(es_localizador(loc1)&&es_localizador(loc2)&&(son_iguales(info_cadena(loc1,c1),info_cadena(loc2,c2)))){  
      //aunque info_cadena tiene como precondicion localizador_en_cadena,
      // loc1 y loc2 ya estan definidas para que esten en la cadena
      loc1 = siguiente(loc1,c1);
      loc2 = siguiente(loc2,c2);
    }//while
    return ((!es_localizador(loc1)&&(!es_localizador(loc2))));
    //la unica forma de que las cadenas sean iguales es que ambos localizadores hayan llegado al null al mismo tiempo
  }//else
}

/*
  Devuelve el resultado de concatenar `c2' después de `c1'.
  La cadena_t resultado no comparte memoria ni con `c1' ni con `c2'.
 */
cadena_t concatenar(cadena_t c1, cadena_t c2){
  cadena_t nuevo = crear_cadena();
    info_t nuevainfo;
        
  if (es_vacia_cadena(c1)&&es_vacia_cadena(c2))
  {
    return nuevo;
  }
  else
  {
            localizador_t loc1 = inicio_cadena(c1);

            while(es_localizador(loc1)){ 
                    nuevainfo = copia_info(info_cadena(loc1, c1));
                    insertar_al_final(nuevainfo, nuevo);
                    loc1=siguiente(loc1,c1);
    }//while

    localizador_t loc2 = inicio_cadena(c2);

            while(es_localizador(loc2))
            {
                nuevainfo = copia_info(info_cadena(loc2, c2));
                insertar_al_final(nuevainfo, nuevo);
                loc2=siguiente(loc2,c2);
            }//while
            
            return nuevo;
  }//else 
}

/*
  Se ordena `cad' de manera creciente según los datos numéricos de sus
  elementos.
  No se debe obtener ni devolver memoria de manera dinámica.
  Si es_vacia_cadena (cad) no hace nada.
 */
void ordenar(cadena_t &cad)
{
    //si no es vacia, entonces la ordeno
    if (!es_vacia_cadena(cad)) 
    {
        localizador_t loc = inicio_cadena(cad);
        localizador_t aux;
        localizador_t minimo;
        info_t min;
        while (loc != NULL) {
            aux = loc;
            minimo = loc;
            //mientras aux es distinto de NULL, recorro a partir de loc con aux
            while (aux != NULL)
            {
                //si el numero de aux es menor al de minimo, entonces minimo pasa ser aux
                if (numero_info(info_cadena(aux, cad)) < numero_info(info_cadena(minimo, cad)))
                    minimo = aux;
                //avanzo con aux
                aux = siguiente(aux, cad);
            }
            //guardo el dato del minimo en min
            min = info_cadena(minimo, cad);
            //en el lugar del minimo, pongo el dato de loc
            cambiar_en_cadena(info_cadena(loc, cad), minimo, cad);
            //en donde estaba loc, pongo el dato del minimo
            cambiar_en_cadena(min, loc, cad);
            //avanzo loc
            loc = siguiente(loc, cad);
            
        }
    }
}

/*
  Cambia todas las ocurrencias de `original' por `nuevo' en los elementos
   de `cad'. No debe quedar memoria inaccesible.
 */
void cambiar_todos(int original, int nuevo, cadena_t &cad)
{
  localizador_t loc = inicio_cadena(cad);

  while(es_localizador(loc))
  {
    if ((numero_info(info_cadena(loc,cad)))== original)
    {
      char *copiafrase = new char[strlen(frase_info(info_cadena(loc, cad)))+1];
      strcpy(copiafrase, frase_info(info_cadena(loc, cad)));
      info_t nuevodato = crear_info(nuevo, copiafrase);
      info_t borrar=info_cadena(loc, cad);
      liberar_info(borrar);
      cambiar_en_cadena(nuevodato, loc, cad);
    }

      loc = siguiente(loc,cad);
  }

}

/*
  Devuelve la cadena_t de elementos de `cad' que cumplen
  "menor <= numero_info (elemento) <= mayor".
  El orden relativo de los elementos en la cadena_t resultado debe ser el mismo
  que en `cad'.
  La cadena_t resultado no comparte memoria con `cad'.
  Precondición: esta_ordenada (cad), `menor' <= `mayor',
  pertenece (menor, cad), pertenece (mayor, cad).
 */
cadena_t subcadena(int menor, int mayor, cadena_t cad)
{
  localizador_t loc = inicio_cadena(cad);
  while(numero_info(info_cadena(loc,cad))< menor)
    loc=siguiente(loc,cad);
  //me muevo hasta encontrar el primero que cumple que sea mayor o igual a menor

  cadena_t nuevo = crear_cadena();

  while(loc!=NULL && numero_info(info_cadena(loc,cad))<=mayor)
  {
    insertar_al_final(copia_info(info_cadena(loc, cad)),nuevo); 
    //le pongo el elemento a cad;
    loc=siguiente(loc,cad);

  }
  return nuevo;
}
