/* 5220298 - 5034511  */

#include "../include/info.h"
#include "../include/cadena.h"
#include "../include/binario.h"
#include "../include/uso_tads.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

struct rep_binario {
	info_t dato;
	rep_binario *izq;
	rep_binario *der;
};

/* **************  NUEVO  ************** */
/*
  Devuelve un árbol balanceado cuyo elementos son los que están contenidos en
  `cad'.
  En esta función se dice que un árbol está balanceado si
  a) es vacío;
  o
  b)
    b1) el sub-árbol izquierdo tiene la misma cantidad de elementos o uno más
      que el  subárbol derecho;
    y
    b2) los subárboles izquierdo y derecho están balanceados.

  DEFINICIÓN ALTERNATIVA DE ÁRBOL BALANCEADO
  En esta función se dice que un árbol esta balanceado si en cada nodo, la
  cantidad de elementos de su subárbol izquierdo es igual a, o 1 más que, la
  cantidad de elementos de su subárbol derecho.

  Precondición: los elementos de `cad' están en orden lexicográfico creciente
  estricto según sus datos de texto.
  El árbol devuelto no comparte memoria con `cad'.
  El tiempo de ejecución es O(n . log n), siendo `n` la cantidad de elementos
  de `cad'.
 */

/* Funcion que dada una cadena, devuelve una copia de ella, sin compartir ningun tipo 
de memoria. Tiene O(n).
*/

static cadena_t copiar_cadena(cadena_t cad)
{
  cadena_t copia = crear_cadena();
  localizador_t loc = inicio_cadena(cad);

  while (es_localizador(loc))
  {
    insertar_al_final(copia_info(info_cadena(loc,cad)),copia);
    loc=siguiente(loc,cad);
  }

  return copia;
}

/*funcion auxiliar para crear_balanceado*/

static binario_t aux_crear_balanceado(cadena_t &cad, unsigned int cuenta)
{
  if (es_vacia_cadena(cad)|| cuenta==0)
    return crear_binario();
  else
  { unsigned int i=cuenta/2;
    localizador_t kes_nodo= kesimo(i,cad);
    cadena_t cad_izq,cad_der;
    localizador_t inicio = inicio_cadena(cad);
    binario_t res=new rep_binario;
    
    if (es_localizador(kes_nodo))
    {
        cad_izq = separar_segmento(inicio, kes_nodo, cad);
        res->izq=aux_crear_balanceado(cad_izq, i);
    }    
    else
      res->izq=NULL;


    inicio = inicio_cadena(cad);
    res->dato=(copia_info(info_cadena(inicio,cad)));

    localizador_t sig  = siguiente(inicio,cad);
    localizador_t fin = final_cadena(cad);
    
    if (es_localizador(sig))
    {
        cad_der= separar_segmento(sig, fin, cad);
        res->der=aux_crear_balanceado(cad_der, cuenta-(i+1)); 
    }
    else res->der = NULL;
    
    liberar_cadena(cad);
        
    
    
    return res;


  }

}

binario_t crear_balanceado(cadena_t cad)
{
  if (es_vacia_cadena(cad))
    return NULL;
  else
  {
    unsigned int cuenta = longitud(cad);
    cadena_t copia = copiar_cadena(cad);

    binario_t bin = aux_crear_balanceado(copia, cuenta);
    //liberar_cadena(copia);
    return bin;

  }
}



/* Constructoras */

/*  Devuelve un binario_t vacío (sin elementos). */
binario_t crear_binario()
{
	return NULL;
}

/*
  Inserta `i' en `b' respetando la propiedad de orden definida.
  Si en `b' ya hay un elemento cuyo dato de texto es frase_info(i) no se hace
  nada.
  Devuelve `true' si se insertó `i', o `false' en otro caso.
 */
bool insertar_en_binario(info_t i, binario_t &b)
{
  //si es vacio, creo la raiz
  if (es_vacio_binario(b))
  {
    //pido memoria
    b=new rep_binario;
    //asigno i al dato de b
    b->dato=i;
    b->izq=NULL;
    b->der=NULL;
    return true;
  }
  else
  {
    if ((strcmp(frase_info(b->dato),frase_info(i)))<0)
   //si strcmp<0, la frase de b->dato es mas chica que la frase de i
      //entonces inserto i en el sub arbol derecho de b
      return insertar_en_binario(i,b->der);
    else if ((strcmp(frase_info(b->dato),frase_info(i)))>0)
      //si strcmp>0, la frase de b->dato es mas grande que la de i
      //entonces inserto i en el sub arbol izquierdo de b
        return insertar_en_binario(i,b->izq);
    else
     //si las frases son iguales, no lo inserto (devuelvo false)
        return false;
  }
  
}


/* Destructoras */

/*
  Devuelve el elemento mayor (según la propiedad de orden definida) de `b'.
  Remueve el nodo en el que se localiza ese elemento (libera la memoria
  asignada al nodo, pero no la del elemento).
  Precondición: ! es_vacio_binario(b).
 */
info_t remover_mayor(binario_t &b)
{
	info_t res;
	if (b->der == NULL)
	{
		res = b->dato;
		binario_t izq = b->izq;
		delete b;
		b = izq;
	}
	else
	{
		res = remover_mayor(b->der);
	}
	return res;
}


/*
  Remueve de `b' el nodo en el que el dato de texto de su elemento es `t'.
  Si en ningún nodo se cumple esa condición no se hace nada.
  Si los dos subárboles del nodo a remover son no vacíos, en sustitución del
  elemento removido debe quedar el que es el mayor (segun la propiedad de orden
  definida) en el subárbol izquierdo.
  Libera la memoria del nodo y del elemento.
 */
bool remover_de_binario(const char *t, binario_t &b)
{
	if(b != NULL)
	{
		if (strcmp(frase_info(raiz(b)),t) == 0) 
		//si estoy en el elemento que quiero 
		{	
      info_t raiz_dato=raiz(b);
			liberar_info(raiz_dato);
      binario_t aux;
      if ((b->izq != NULL)&&(b->der != NULL ))
        b->dato= remover_mayor(b->izq);
      else if (b->izq == NULL)
      {
        aux = b;
        b=b->der;
        delete aux;
      }
      else 
      {
        aux =b;
        b = b->izq;
        delete aux;
      }
    return true;
    }
		else if(strcmp(frase_info(raiz(b)),t) > 0)
			//si donde estoy es mas grande de lo que quiero
			  return remover_de_binario(t, b->izq);

		else 
			//si donde estoy es mas chico de lo que quiero
			  return remover_de_binario(t, b->der);
	}
  else 
    return false;
}

/* Libera la memoria asociada a `b' y todos sus elementos. */
void liberar_binario(binario_t &b)
{
	if (b !=NULL)
	{
		liberar_binario(b->izq);
		liberar_binario(b->der);
    info_t raiz_b=raiz(b);
		liberar_info(raiz_b);
		delete b;
    b=NULL;
	}
}

/* Predicados */

/* Devuelve `true' si y sólo si `b' es vacío (no tiene elementos). */
bool es_vacio_binario(binario_t b)
{
	return (b == NULL);
}



static nat max(nat b1, nat b2)
{
  if (b1 >= b2) 
    return b1;
  else
    return b2;
}

/*Funcion que dados dos naturales a y b, me devuelve el valor absoluto de a-b*/
static nat AbsValResta(nat a, nat b)
{
  if (a>b)
    return a-b;
  else return b-a;
}


/*Funcion auxiliar para AVL
Precondicion: !es_vacio_binario(b)*/

static bool aux_AVL(binario_t b, nat cuenta, nat &nivel)
{
  
  /*Si el subarbol derecho y el izquierdo son vacios, estoy en una hoja*/
  if ((b->der==NULL)&&(b->izq==NULL))
  {
    //guardo en nivel el valor de cuenta
    nivel=cuenta;
    //las hojas siempre son AVL, asi que devuelvo true
    return true;
  }

  else 
  {
    //si el subarbol derecho es vacio
    if (b->der==NULL)
    {
      //la "altura" del subarbol derecho es 0 
      nat alturader=0;
      //me fijo si el subarbol izquierdo es AVL
      bool AVL_izq=aux_AVL(b->izq,cuenta+1,nivel);
      //alturaizq cuenta cuantos niveles avance con la recursion en el subarbol izquierdo
      //es decir, la altura del subarbol izquierdo
      nat alturaizq=nivel-cuenta;
      //si el izquierdo era AVL y la diferencia entre las alturas de los subarboles es menor o igual a 1, devuelvo true
      return (AVL_izq && (AbsValResta(alturaizq,alturader)<=1));
    }
    else 
    {
      //si el subarbol izquierdo es vacio
      if (b->izq==NULL)
      {
        //la "altura" del subarbol izquierdo es 0
        nat alturaizq=0;
        //me fijo si el subarbol derecho es AVL
        bool AVL_der=aux_AVL(b->der,cuenta+1,nivel);
        nat alturader=nivel-cuenta; 
        //si el derecho era AVL y la diferencia entre las alturas de los subarboles es menor o igual a 1, devuelvo true
        return (AVL_der && (AbsValResta(alturader,alturaizq)<=1));
      }
      else
      { 
        //me fijo si el izquierdo es AVL
        bool AVL_izq=aux_AVL(b->izq,cuenta+1,nivel);
        //la altura del izquierdo es los niveles que avance en la recursion, es decir el nivel-cuenta
        nat alturaizq=nivel-cuenta;
        //me fijo si el izquierdo es AVL
        bool AVL_der=aux_AVL(b->der,cuenta+1,nivel);
        //la altura del derecho es los niveles que avance en la recursion, es decir nivel-cuenta
        nat alturader=nivel-cuenta;
        //reseteo el nivel al maximo de los dos (le sumo cuenta porque se la habia restado)
        nivel=max(alturader,alturaizq)+cuenta;
        //devuelvo true sii el izquierdo y el derecho son AVL, y la diferencia de las alturas es menor o igual a 1
        return (AVL_izq && AVL_der && (AbsValResta(alturaizq,alturader)<=1));

      }
    }
  }
}

/*
  Devuelve `true' si y sólo si cada nodo de `b' cumple la condición de balanceo
  AVL. El árbol vacío cumple la condición.
  Un nodo cumple la condición de balanceo AVL si el valor absoluto de la
  diferencia de las alturas de sus subárboles izquierdo y derecho en menor o
  igual a 1.
  Cada nodo se puede visitar una sola vez. */
bool es_AVL(binario_t b)
{
  //el arbol vacio cumple con la condicion
  if (es_vacio_binario(b))
    return true;
  else
    { nat nivel=0; 
      return aux_AVL(b,0, nivel);
    }
}


/* Selectoras */

/*
  Devuelve el elemento asociado a la raíz de `b'.
  Precondición: ! es_vacio_binario(b).
 */
info_t raiz(binario_t b)
{
	return b->dato;
}

/*
  Devuelve el subárbol izquierdo de  `b'.
  Precondición: ! es_vacio_binario(b).
 */
binario_t izquierdo(binario_t b)
{
	return b->izq;
}

/*
  Devuelve el subárbol derecho de  `b'.
  Precondición: ! es_vacio_binario(b).
 */
binario_t derecho(binario_t b)
{
	return b->der;
}

/*
  Devuelve el subárbol que tiene como raíz al nodo con el elemento cuyo dato de
  texto es `t'.
  Si `t' no pertenece a `b', devuelve el árbol vacío.
  El tiempo de ejecución es O(log n) en promedio, donde `n' es la cantidad de
  elementos de `b'.
 */
binario_t buscar_subarbol(const char *t, binario_t b)
{
  //si es vacio, t no va a estar asi que devuelvo NULL
	if (es_vacio_binario(b))
    return NULL;
  else  
  {
    //si la frase de b es la misma que t, devuelvo b
    if (strcmp(frase_info(b->dato),t)==0)
      return b;
    else
      //si strcmp<0, la frase de b es mas chica que t, asi que busco t en el subarbol derecho
      if (strcmp(frase_info(b->dato),t)<0)
        return buscar_subarbol(t, b->der);
      else //si es >0, la frase de b es mas grande que t, asi que busco en el subarbol izquierdo
        return buscar_subarbol(t, b->izq);

  
  }
}


/*
  Devuelve la altura de `b'.
  La altura de un árbol vacío es 0.
   El tiempo de ejecución es O(n), donde `n' es la cantidad de elementos de `b'.
 */
nat altura_binario(binario_t b)
{
	if(b == NULL)
		return 0;
	else
		return 1 + max(altura_binario(b->izq),altura_binario(b->der));
}

/*  Devuelve la cantidad de elementos de `b'.
El tiempo de ejecución es O(n), donde `n' es la cantidad de elementos de `b'. */
nat cantidad_binario(binario_t b)
{
	if (b == NULL)
		return 0;
	else
		return 1 + cantidad_binario(b->izq) + cantidad_binario(b->der);
}
/*
Funcion auxiliar para kesimo_en_binario. 
h := contador del kesimo mas chico.
Al principio va hasta el de mas a la izquierda, que es el mas chico, y ahi pone h en 1.
Si el valor que me pedian no era 1, me fijo el mas chico del arbol izquierdo,
y cuando llego hago h++.

*/

static void auxk(binario_t b, nat &k, info_t &nuevo )
{
  if(b != NULL)
  { 
        auxk(b->izq, k, nuevo);
        //cuando llego al mas chico del subarbol, es porque es el siguiente mas chico.
        k = k -1;
        if (k == 0)
           nuevo = raiz(b);
        else
        //voy hasta el mas chico de la derecha
           auxk(b->der, k,nuevo);
  }  
}



/*
  Devuelve el elemento que, según la propiedad de orden de los árboles
  `binario_t', está en el k-ésimo lugar de `b'.
  No se deben crear estructuras auxiliares.
  No se deben visitar nuevos nodos después que se encontró el kesimo.
  Precondición: 1 <= k <= cantidad_binario(b).
  El tiempo de ejecución es O(n), donde `n' es la cantidad de elementos de `b'.
 */
info_t kesimo_en_binario(nat k, binario_t b)
{

    info_t nuevo;
    auxk(b,k,nuevo);
    return nuevo;

}

/*
  Devuelve una cadena_t con los elementos de `b' en orden lexicográfico
  creciente
  según sus datos de texto.
  La cadena_t devuelta no comparte memoria con `b'.
   El tiempo de ejecución es O(n) siendo `n' la cantidad de nodos de `b'.
 */
cadena_t linealizacion(binario_t b)
{
  if (es_vacio_binario(b))
  {
    cadena_t cad=crear_cadena();
    return cad;
  }
	else
	{   
         //en cad tengo linealiacion de b->izq
          cadena_t cadizq = linealizacion(b->izq);
          //copio el dato
          info_t copiadato=copia_info(b->dato);
          //lo inserto al final
          insertar_al_final(copiadato,cadizq);
          //guardo linealizacion(b->der) en siguiente
          cadena_t cadder=linealizacion(b->der);
          localizador_t final=final_cadena(cadizq);
          //inserto cadder al final de cad
          insertar_segmento_despues(cadder,final,cadizq);
          liberar_cadena(cadder);
          return cadizq;

        }

  }
	


/*
  Devuelve un árbol con copias de los elementos de `b' que cumplen la condición
  "numero_info(elemento) < clave".
  La estructura del árbol resultado debe ser análoga a la de `b'. Esto
  significa que dados dos nodos `U' y `V' de `b' en los que se cumple la
  condición y tal que `U' es ancestro de `V', en el árbol resultado la copia de
  `U' debe ser ancestro de la copia de `V' cuando sea posible. Esto no siempre
  se puede lograr y al mismo tiempo mantener la propiedad de orden del árbol
  en el caso en el que en un nodo `V' no se cumple la condición y en sus dos
  subárboles hay nodos en los que se cumple. En este caso, la copia del nodo
  cuyo elemento es el mayor (según la propiedad de orden definida) de los que
  cumplen la condición en el subárbol izquierdo de `V' deberá ser ancestro de
  las copias de todos los descendientes de `V' que cumplen la condición.
  (Ver ejemplos en LetraTarea3.pdf).
  El árbol resultado no comparte memoria con `b'. *)
  El tiempo de ejecución es O(n), donde `n' es la cantidad de elementos de `b'.
 */
binario_t filtrado(int clave, binario_t b)
{
  binario_t filtradoizq, filtradoder, nuevo;
 
  //si b es vacio, devuelvo el arbol vacio
  if (es_vacio_binario(b))
    return NULL;
  else 
  { 
    filtradoizq=filtrado(clave,b->izq);
    filtradoder=filtrado(clave,b->der);
    //si el dato de b cumple la condicion, lo tengo que agregar 
    if (numero_info(b->dato)<clave)
    {
      //pido memoria
      nuevo=new rep_binario;
      //copio el dato y se lo asigno a nuevo->dato
      nuevo->dato=copia_info(b->dato);
      //asigno los subarboles filtrados de b a nuevo
      nuevo->izq=filtradoizq;
      nuevo->der=filtradoder;
      return nuevo;      
    }
    else //si el dato de b no cumple la condicion
      //si el filtrado derecho me dio null, solo devuelvo el filtrado izquierdo
      if (filtradoder==NULL)
        return filtradoizq;
      else 
        //si el filtrado izquierdo me dio null, solo devuelvo el filtrado derecho
        if (filtradoizq==NULL)
          return filtradoder;
        else //si no, los tengo que combinar
        {
          //pido memoria para la raiz del arbol filtrado
          nuevo=new rep_binario;
          //remuevo el mayor del filtrado izquierdo, y su dato queda como raiz
          nuevo->dato=remover_mayor(filtradoizq); 
          //asigno el filtrado derecho e izquierdo a nuevo
          nuevo->izq=filtradoizq;
          nuevo->der=filtradoder;
          return nuevo;

        }


  }


} 

/* Salida */
/*


*/
static void print_height (nat alt_g, binario_t b,nat k )
{
 if(b != NULL)
 {
    print_height(alt_g,b->der,k+1);
    for (nat i = 1;i <= k;i++)
      printf("-");
    char *str = info_a_texto(raiz(b));
    printf("%s", str);
    printf("\n");
    delete[] str;
    
    print_height(alt_g,b->izq,k+1);

  }
} 
/*
  Imprime los elementos de `b', uno por línea, en orden descendente de la
  propiedad de orden de los árboles `binario_t'.
  Antes del elemento imprime una cantidad de guiones igual a su nivel.
  El elemento se imprime según la especificación dada en `info_a_texto'.
  El nivel de la raíz es 0.
  Antes de terminar, se debe imprimir un fin de linea.
  Si es_vacio_binario(b) sólo se imprime el fin de línea.
   El tiempo de ejecución es O(n), donde `n' es la cantidad de elementos de `b'.
 */
void imprimir_binario(binario_t b)
{
  if (b != NULL)
  {
    nat alt_g = altura_binario(b); //altura general
    printf("\n" );
    print_height(alt_g,b,0);

  }
  else
    printf("\n");
}
