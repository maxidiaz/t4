/* 5220298 - 5034511  */

#include "../include/cadena.h"
#include "../include/iterador.h"
#include "../include/info.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
//FUNCIONA

struct nodo_iter{
  char * str;
  nodo_iter *anterior, *siguiente;
};

typedef nodo_iter *lista_iter;

struct rep_iterador{
  lista_iter inicio, final;
  lista_iter pos_actual;
};


/*
  Crea un iterador_t vacío (sin elementos) cuya posición actual no está
  definida.
 */
iterador_t crear_iterador()
{
    iterador_t iter = new rep_iterador;
    iter->pos_actual = iter->inicio = NULL;
    iter->final = NULL;

    return iter;
}

/*
  Agrega `t' al final de `i'. No se modifica la posición actual.
 */
void agregar_a_iterador(char *t, iterador_t &i)
{
        lista_iter nuevo = new nodo_iter;
        nuevo->str = t;
        nuevo->siguiente = NULL;
        nuevo->anterior = i->final;
        if (i->final == NULL) 
            i->inicio = nuevo;
        else 
            i->final->siguiente = nuevo; 
        i->final = nuevo;
}

/*
   Mueve la posición actual al primero.
   Si en `i' no hay elementos no hace nada.
 */
void reiniciar_iter(iterador_t &i)
{
  i->pos_actual = i->inicio;
}

/*
  Avanza la posición actual hacia el siguiente.
  Si ! hay_siguiente_en_iter(i) la posición actual queda indefinida;
  Si la posición actual no está definida no hace nada.
 */
void avanzar_iter(iterador_t &i)
{
  if (i->pos_actual != NULL)
  {
    i->pos_actual=i->pos_actual->siguiente;
  }
}

/*
  Devuelve el elemento de la posición actual.
  Precondición: esta_definida_actual(i).
 */
char *actual_iter(iterador_t &i)
{
	return i->pos_actual->str;
}

/*
  Devuelve `true' si y sólo si en `i' hay un elemento siguiente al de la
  posición actual.
  Precondición: esta_definida_actual(i).
 */
bool hay_siguiente_en_iter(iterador_t i)
{
    return (i->pos_actual->siguiente != NULL);
}

/*
  Devuelve `true' si y sólo si la posición actual de `i' está definida.
 */
bool esta_definida_actual(iterador_t i)
{
	return (i->pos_actual != NULL);
}

/*
  Devuelve una cadena con todos los elementos que se pueden recorrer con `i'.
  El dato numérico de los elementos debe ser 0.
  Al terminar, la posicion actual debe permanecer donde estaba al empezar la
  operación.
  La cadena resultado no debe compartir memoria con `i'.
 */
cadena_t iterador_a_cadena(iterador_t i)
{
	cadena_t nueva = crear_cadena();
	lista_iter posicion = i->pos_actual;
	reiniciar_iter(i);
	while (esta_definida_actual(i))
	{
		char *frase = new char[strlen(i->pos_actual->str)+1];
		strcpy(frase,i->pos_actual->str);
		info_t info = crear_info(0, frase);
		insertar_al_final(info, nueva);
		avanzar_iter(i);
	}
	i->pos_actual = posicion;
	return nueva;

}
static void aux_liberar_iterador(lista_iter l)
{
  if (l != NULL)
  { 
    aux_liberar_iterador(l->siguiente);
    delete[] l->str;
    delete l;
  }
}
/*  Libera la memoria asignada a `i' y la de sus elementos). */
void liberar_iterador(iterador_t &i)
{
  aux_liberar_iterador(i->inicio);
  delete i;
  i=NULL; 
}
