//Error: timeout: the monitored command dumped core Segmentation fault


static binario_t aux_crear_balanceado(cadena_t &cad, unsigned int cuenta)
{
  if (es_vacia_cadena(cad)|| cuenta==0)
    return crear_binario();
  else
  { unsigned int i=cuenta/2;
    localizador_t kes_nodo= kesimo(i,cad);
    cadena_t cad_izq, cad_der;
    localizador_t inicio = inicio_cadena(cad);
    localizador_t final = final_cadena(cad);

    if (es_localizador(kes_nodo))
      cad_izq = separar_segmento(inicio, kes_nodo, cad);
    else
      cad_izq = crear_cadena();


    binario_t res=new rep_binario;
    res->dato=(copia_info(info_cadena(inicio,cad)));

    remover_de_cadena(inicio,cad);

    if (!es_vacia_cadena(cad))
    {
      inicio=inicio_cadena(cad);
      cad_der = separar_segmento(siguiente(inicio,cad),final,cad);
    }  

    else cad_der = crear_cadena();

    res->izq=aux_crear_balanceado(cad_izq, i);
    res->der=aux_crear_balanceado(cad_der, cuenta-(i+1));
  
    
    return res;


  }

}