static binario_t aux_crear_balanceado(cadena_t cad, unsigned int cuenta)
{
  if (es_vacia_cadena(cad)|| cuenta==0)
    return NULL;
  else
  { unsigned int i=cuenta/2;
    localizador_t aux= kesimo(i,cad);
    localizador_t hasta;
    cadena_t cad_izq, cad_der;
    binario_t res=new rep_binario;
    if (es_localizador(aux) && es_localizador(siguiente(aux,cad)))
    {
      cad_izq = segmento_cadena(inicio_cadena(cad), aux, cad);
      hasta = siguiente(aux,cad);
      
    }
    else
    { 
      cad_izq = crear_cadena();
      hasta = inicio_cadena(cad);
    }

    res->dato=(copia_info(info_cadena(hasta,cad)));

    if (es_localizador(siguiente(hasta,cad)))
      cad_der = segmento_cadena(siguiente(hasta,cad),final_cadena(cad),cad);
  
    else cad_der = crear_cadena();

    res->izq=aux_crear_balanceado(cad_izq, i);
    res->der=aux_crear_balanceado(cad_der, cuenta-(i+1));
    liberar_cadena(cad_izq);
    liberar_cadena(cad_der);
    
    return res;


  }

}

binario_t crear_balanceado(cadena_t cad)
{
  if (es_vacia_cadena(cad))
    return NULL;
  else
  {
   localizador_t pos=inicio_cadena(cad);
    unsigned int cuenta = 0;
    while (es_localizador(pos))
    {
      cuenta++;
      pos = siguiente(pos,cad);
    }
    return aux_crear_balanceado(cad, cuenta);
  }
}